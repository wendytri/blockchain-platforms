### Purpose ###
This repository contains documentation of a few blockchain platforms, namely:
1. Nxt
2. Lisk
3. Ethereum
4. Multichain

### Reference ###
1. Nxt
https://nxt.org/what-is-nxt/voting/

2. Lisk
https://lisk.io/

3. Ethereum
https://www.ethereum.org/

4. Multichain
http://www.multichain.com/

### Final Decision ###
After discussing and comparing each platform, Multichain was chosen to be the most appropriate blockchain platform for our project.